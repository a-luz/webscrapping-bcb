(ns main
  (:require
   [clj-http.client :as client]
   [hickory.core :refer :all]
   [hickory.select :as s]))


(def url "https://www3.bcb.gov.br/CALCIDADAO/publico/corrigirPorIndice.do")

(defn make-request
  [init-date]
  (let [payload {:aba 1
                 :dataInicial init-date
                 :dataFinal "01/2023"
                 :selIndice "00433IPCA"
                 :valorCorrecao "437,00"}
        headers {"Host" "www3.bcb.gov.br"
                 "Origin" "https://www3.bcb.gov.br"
                 "Content-Type" "application/x-www-form-urlencoded"
                 "Accept-Encoding" "gzip, deflate, br"
                 "Connection" "keep-alive"
                 "User-Agent" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/109.0.0.0 Safari/537.36"
                 "Referer" "https://www3.bcb.gov.br/CALCIDADAO/publico/exibirFormCorrecaoValores.do?method=exibirFormCorrecaoValores&aba=1"
                 "Accept" "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8"}
        opts {:query-params {:method "corrigirPorIndice"} :form-params payload :redirect-strategy :lax :trace-redirects true :content-type :x-www-form-urlencoded}]
    (:body (client/post url {:query-params {:method "corrigirPorIndice"} :form-params payload :redirect-strategy :lax :trace-redirects true :content-type :x-www-form-urlencoded}))))


(defn parse-result
  [result]
  (as-hickory
   (parse result)))

(defn parse-monetary-string
  [mstring]
  (let [format (java.text.NumberFormat/getNumberInstance java.util.Locale/GERMANY)]
    (->>
     (clojure.string/split mstring #" ")
     rest
     second
     (.parse format))))

(defn grab-corrected-money-string
  [site-tree]
  (-> (s/select (s/child (s/tag :tr)
                         s/first-child
                         (s/attr :class)
                         (s/attr :style)
                         (s/tag :tbody)
                         s/last-child)
                site-tree)
      first :content rest rest second :content first))


(def date-vector
  (->>
   (map first
        (for [year  (range 2018 2024)
              month (range 1 13)]
          [(str (format "%02d" month) "/" year)]))
   rest rest rest (take 58)))

(defn main-loop
  []
  (map (fn [date]
         (-> date
             make-request
             parse-result
             grab-corrected-money-string
             parse-monetary-string
          ))
       date-vector))


(comment
  (apply + (main-loop)) ;;29752.44
  (* 57 437) ;; 24909
  )
